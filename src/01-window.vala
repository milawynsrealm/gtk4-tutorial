int main(string[] args) {
    /* Create a new application instance */
    var app = new Gtk.Application("org.gtk.example", GLib.ApplicationFlags.FLAGS_NONE);

    /* When the program is first run, do these tasks first */
    app.activate.connect(() => {
        // Create a new window object
        var window = new Gtk.ApplicationWindow(app);

        /* Set up details about the window */
        window.set_title("This is my Vala Test");
        window.set_default_size(350, 160);

        /* Show the final window */
        window.present();
    });

    /* Run the application */
    return app.run(args);
}
