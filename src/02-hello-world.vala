int main(string[] args) {
    /* Create a new application instance */
    var app = new Gtk.Application("org.gtk.example", GLib.ApplicationFlags.FLAGS_NONE);

    /* When the program is first run, do these tasks first */
    app.activate.connect(() => {
        // Create a new window object
        var window = new Gtk.ApplicationWindow(app);
        var button = new Gtk.Button.with_label("Click Me!");

        /* Set up details about the window */
        window.set_title("Button Test");
        window.set_default_size(350, 160);

        /* When the button is clicked, do this stuff */
        button.clicked.connect(() => {
            /* Create a message box as a response */
            var msg_hello = new Gtk.MessageDialog(window, DESTROY_WITH_PARENT, INFO, OK, "Hello World!");

            /* When the user clicks on the OK button, actually close the dialog box */
            msg_hello.response.connect(() => {
                msg_hello.close();
            });

            /* Show the final button */
            msg_hello.show();
        });

        /* Attach the button to the window */
        window.set_child(button);

        /* Show the final window */
        window.present();
    });

    /* Run the application */
    return app.run(args);
}
