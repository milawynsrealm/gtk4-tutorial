# GTK 4.0 for VALA Tutorial

Although it’s been almost two years since it’s official release at the time of writing ([December 16, 2020](https://blog.gtk.org/2020/12/16/gtk-4-0/)), you would think that it would have a lot of good tutorials out there for the GTK 4.0 library, especially for the VALA language. Since I started learning programming for the GNU/Linux platform after the 4.0 release, I currently have no good reason for learning the previous versions of this library (e.g. GTK2 and GTK3). But since 4.0 and later has some serious structural changes made to it, and since most existing tutorials are for older versions, I only have fragments to work with. With that being said, I have compiled a tutorial series to hopefully fill in these gaps for myself. I have taken what I could from existing documentation such as [valadocs](https://valadoc.org/index.htm) and other [tutorials](https://docs.gtk.org/gtk4/getting_started.html) about GTK in general, so it should at least be a good starting point for developing GTK 4 with VALA.

These tutorials assume you already have [VALA](https://wiki.gnome.org/Projects/Vala/ValaPlatforms) and [GTK4](https://gtk.org/) already installed and set up on your computer.

## Table of Contents ##
[Lesson 1](./tutorial/01-window.md) - Create a basic GUI window.<br>
[Lesson 2](./tutorial/02-hello-world.md) - Create a simple button that shows a simple message.<br>
[Lesson 3](./tutorial/03-layout.md) - Create buttons using the Layout Manager.<br>
