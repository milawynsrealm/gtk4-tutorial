# Lesson 3 - Using the Layout Manager #

![Simple button example with response](../images/03-grid.jpg)

Having a button to interact with is fine and all, but at some point your real world application will most likely require more than one control to work with. Having a way to lay everything out on a window without having to manually position everything. The programmer tells the program how it wants everything to be laid out, and the GTK 4 should do the rest. For more detailed information about layout managers, click [here](https://blog.gtk.org/2019/03/27/layout-managers-in-gtk-4/).

In the main application connection, <code>app.activate.connect()</code>, after creating the window, you need to first create a grid object so we can put our widgets into. In this case, we are putting in three buttons.

```Vala
/* Create a grid to attach the buttons to */
var grid = new Gtk.Grid();
```

After creating the grid, you need to attach the grid to the window so it shows up properly to the user.

```Vala
/* Attach the grid to the main window */
window.set_child(grid);
```

Next, you need to create a new button after attaching the currently set up one. Then, you attach the created button to the existing grid before creating another button using the same button object. It is basically like a spreadsheet table that you are filling up.

<b>
<table style="border:2px solid black;">
    <tr>
        <td>Button 1 (0, 0, 1, 1)</td>
        <td>Button 2 (1, 0, 1, 1)</td>
    </tr>
    <tr>
        <td colspan="2">Quit (0, 1, 2, 1)</td>
    </tr>
</table>
</b>

Please note for the exit button, the width is marked as 2 units instead of just 1. This means that the button takes up two spaces instead of just one. Just like how you would merge and center two or more cells into one in any spreadsheet application.

```Vala
/* Create the second button. The first one is replaced
* after adding it to the grid. */
button = new Gtk.Button.with_label("Button 2");

/* When the user clicks on the second button, show a message */
button.clicked.connect(() => {
    stdout.printf("Hello World 2!\n");
});

/* Attach button to the grid */
/* Function layout: child, column, row, width = 1, height = 1 */
grid.attach(button, 0, 0, 1, 1);
```

Hopefully, this will be a good starting point as the layout manager is very complicated, and cannot be easily discussed in a single tutorial.

Click [here](../src/03-grid.vala) for the final code.<br>

([Back to Main](../README.md))
