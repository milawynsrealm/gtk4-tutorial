# Lesson 1 – Window #

![Image of a simple window](../images/01-window.jpg)

To begin, I am simply creating a simple GUI window that essentially does nothing except close when the user exits out of it. First, we create the main program function that is required by all VALA programs:

```Vala
int main(string[] args) {
    return 0;
}
```

Although there is a way to put the main function inside of a class, for the simplicity of this tutorial, we are using this method for now. Although as the programs get more complicated, classes will be eventually used to make programming a bit easier.

Next, we create a new GTK application by creating a new Application object as shown below. This class automatically handles GTK initialization and a list of other tasks that makes development easier to develop and maintain.

```Vala
var app = new Gtk.Application(“org.gtk.example”, GLib.ApplicationFlags.FLAGS_NONE);
```

The first argument is a name that is meant to be unique to the application and makes it easier for application uniqueness and the second argument are any flags to modify the behavior of the application that is being created like running the program as a service.

```Vala
/* When the program is first run, do these tasks first */
app.activate.connect(() => {
/* Create a new window object */
    var window = new Gtk.ApplicationWindow(app);

    /* Set up details about the window */
    window.set_title("This is my Vala Test");
    window.set_default_size(350, 160);

    /* Show the final window */
    window.present();
});
```

The next step is to create a signal for when the application first runs (app.activate.connect(() =>). If you are familiar with GLib and the C language, than this would be functionally similar to the function, **g_signal_connect**() except the function can possibly be called within the existing function. Inside the signal is the code that is executed when the program first runs, which in this case is to create a window, set up the window caption and initial window size, and then show it to the user.

Click [here](../src/01-window.vala) for the final code.<br>

([Back to Main](../README.md))
