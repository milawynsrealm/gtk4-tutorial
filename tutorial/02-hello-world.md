# Lesson 2 - Hello World #

![Simple button example with response](../images/02-hello-world.jpg)

At some point, you'll want your program to actually do something instead of just showing a blank window. You might want it to calculate an equation, or even manage an entire database. Here though, we're just focused on drawing a button, and getting the program to show the user a response when clicked on.

Overall, it uses the program for the first lesson as a foundation, but here, we add on a few features to actually make it work correctly. First, we create a button right after we create our window. As shown, it creates it right away with a label, but that alone isn't enough to show it on the window.

```Vala
var window = new Gtk.ApplicationWindow(app);
var button = new Gtk.Button.with_label("Click Me!"); // <= ADD THIS LINE
```

Next, we have to create a signal for the button that tells the program what to do when the user clicks on it. In this case, it show a message box telling the user "Hello World!" But unlike, for example, in the Win32 API where all you have to do is call it and it'll take care of most of it, here you have to set up a response signal for the dialog box to get it to close itself if the button OK is pressed. Then after setting up the dialog's behavior, it needs to be shown so the user can interact with it.

```Vala
button.clicked.connect(() = > {
    /* Create a message box as a response */
    var msg_hello = new Gtk.MessageDialog(window, DESTROY_WITH_PARENT, INFO, OK, "Hello World!");

    /* When the user clicks on the OK button, actually close the dialog box */
    msg_hello.response.connect(() => {
        msg_hello.close();
    });

    /* Show the final button */
    msg_hello.show();
});
```

The last step you need before you can show the final window is to attach the button to the window so it actually shows up when you finally present the window to the user.

```Vala
/* Attach the button to the window */
window.set_child(button); // <= ADD THIS LINE

/* Show the final window */
window.present();
```

Click [here](../src/02-hello-world.vala) for the final code.<br>

([Back to Main](../README.md))
